# Simple Boilerplate for New Projects in JS

A personal starter template for simple JS Applications providing *ESLint* linter (following *airbnb* style guide), with *Mocha* for testing (using *ChaiJS* as assertion library) with support for ES6+. 
Also, provides a MIT license, and well, you can use this as a basic readme template :p.

## Getting Started
### Prerequisites

You need to have installed:
- A Javascript Runtime; *NodeJS* (>v8.0.0), which can be acquired from: https://nodejs.org/
- Some Package Manager. I will be using *NPM* here, which is provided by *NodeJS*.
- A Version Control System. I will be using *Git* here, which can be acquired from: https://git-scm.com/

### Installing

Open a terminal console and type line by line (Except comments) the following:

``` bash
# Copy this repository to your Hard Drive
$ git clone git@gitlab.com:Francute/simple-js-boilerplate.git my-new-project-folder-name
$ cd my-new-project-folder-name
# Install required dependencies
$ npm install
```

### Running tests

I provided a simple unit test case to know if everything works as intended. To run it, just type in a terminal console:

``` bash
# Run unit tests
$ npm test
```

If everything works as expected, it should output something similar to:

```
Boilerplate
  √ works
    
1 passing (1ms)
```

## Setting up WebStorm IDE (by JetBrains)

This will make a bit pleasurable your work.

### 1. Add support for EcmaScript 6

Open ```File -> Settings```. Then, on left side of the new window, select ```Languages & Frameworks -> JavaScript```. Lastly, on *JavaScript language version*, select *ECMAScript 6* and *Apply*.

### 2. Enable ESLint

Open ```File -> Settings```. Then, on left side of the new window, select ```Languages & Frameworks -> JavaScript -> Code Quality Tools -> ESLint```. Then:

- Check the *Enable* box
- In *ESLint package:* the path should end in *\node_modules\eslint* 
- On *Configuration File:* browse for (clicking on *...*) *.eslintrc.json* file. This should be on the main project folder.
- Save changes by clicking *Apply*.

### 3. Add auto Mocha support with ES6+ for running tests.

Left to *Run project* icon (The green arrow pointing to the right), click on the select box and select *Edit Configurations...*. In this window, look at left side for ```Defaults -> Mocha```. In *Extra Mocha options:* add ```--require babel-core/register```, and *Apply* the changes.

## How to reproduce this boilerplate

- Create a new folder for the project
- Inside it, open a terminal console and run:

 ``` bash
 # Init a new NodeJS project
 $ npm init
 ```
 
- Install the required dev dependencies:

``` bash
$ npm i -D eslint eslint-config-airbnb-base eslint-plugin-import babel-core babel-preset-env mocha chai 
```

- Modify *package.json* adding at the end:

```
,
"babel": {
  "presets": [ "env" ]
}
```

- Create a standard *.gitignore* file. (Specially ignore *node_modules*)
- Copy [this basic eslintrc file to your project](/.eslintrc.json)
- Change entry point *index.js* file to *app/main.js*.
- Modify *package.json* scripts:

```
"start": "node app/main.js",
"test": "mocha --require babel-core/register tests/unit/"
```

## Author

- Francute

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
